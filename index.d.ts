import { IonicNativePlugin } from '@ionic-native/core';

export declare class MailCoreOriginal extends IonicNativePlugin {
    authenticate(hostname, port, username, password, connectionType, success, fail): Promise<any>;
    fetchAttachment(hostname, port, username, password, connectionType, messageUID, partID, partEncoding, filename, success, fail): Promise<any>;
    syncMessages(hostname, port, username, password, connectionType, lastMessageKnownUID, hablaxServerEmail, hablaxEmailsAccepted, success, fail): Promise<any>;
    imapFetchAllMessages(hostname, port, username, password, connectionType, success, fail): Promise<any>;
    smtpSendMessage(hostname, port, username, password, connectionType, recipient, subject, body, attachmentPath, success, fail): Promise<any>;
    listenNewMessages(hostname, port, username, password, connectionType, lastKnownMessageUID, hablaxServerEmail, hablaxEmailsAccepted, success, fail): Promise<any>;
    killListenerOperations(success, fail): Promise<any>;
    updateHablaxServerEmail(hostname, port, username, password, connectionType, hablaxServerEmail, success, fail): Promise<any>;
    updateHablaxEmailsAccepted(hostname, port, username, password, connectionType, hablaxEmailsAccepted, success, fail): Promise<any>;
}

export declare const MailCore: MailCoreOriginal;